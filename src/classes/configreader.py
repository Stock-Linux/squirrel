class ConfigReader:
    def __init__(self):
        self.config_path = "/etc/squirrel.conf"

    def read_config(self):
        file = open(self.config_path, "r")
        
        data = {"branches": {}}

        for line in file:
            if line.startswith("#"):
                continue

            splitted_line = line.split(" ")
            branch = splitted_line[0].strip()
            del splitted_line[0]
            url = splitted_line[0]
            del splitted_line[0]
            path = " ".join(splitted_line).strip()

            data["branches"][branch] = {
                "url": url,
                "path": path
            }

        return data