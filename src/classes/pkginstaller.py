import os
from classes.downloader import Downloader

from classes.pkggetter import PkgGetter
from classes.logger import Logger
from classes.configreader import ConfigReader

class PkgInstaller:
    def __init__(self, pkg, root="/", verbose=True):
        self.root = os.path.abspath(root) + "/"
        self.pkg = pkg
        self.logger = Logger()
        self.config = ConfigReader().read_config()
        self.pkggetter = PkgGetter(self.pkg, root=self.root)
        self.verbose = verbose

    def install(self):
        # Get package infos
        pkg_infos = self.pkggetter.get_pkg_infos(root=self.root)
        # Get package runtime dependencies
        pkg_deps = self.pkggetter.get_all_rundeps(root=self.root)
        
        # Check if the package is already installed
        if self.is_installed(self.root):
            if self.verbose:
                self.logger.log_info("Package " + self.pkg + " is already installed.")
            return

        pkg_branch = self.pkggetter.get_pkg_branch(root=self.root)
        

        # Package is not installed, install it
        if self.verbose:
            self.logger.log_info("Installing package " + self.pkg + "...")
        # But first, install all runtime dependencies
        for pkg_dep in pkg_deps:
            pkg_dep_installer = PkgInstaller(pkg_dep, root=self.root, verbose=False)
            pkg_dep_installer.install()
        # Then, install the package
        pkg_branch = self.pkggetter.get_pkg_branch(root=self.root)
        pkg_downloader = Downloader(self.config["branches"][pkg_branch]["url"] + "/bins/" + self.pkg + "-" + pkg_infos["version"] + ".tar.xz", self.root + self.pkg + "-" + pkg_infos["version"] + ".tar.xz")
        pkg_downloader.download()
        # The archive is downloaded
        # It is formatted like this: root/usr root/etc root/bin... so we need to extract it in the root directory with a --strip-components=1
        # Cd to the root directory
        os.chdir(self.root)
        os.system("tar -xhpf " + self.pkg + "-" + pkg_infos["version"] + ".tar.xz --strip-components=1")
        # Now, we need to move the tree(alias .[package]_TREE) to the branch path
        os.rename("." + pkg_infos["name"] + "_TREE", "." + self.config["branches"][pkg_branch]["path"] + "/." + pkg_infos["name"] + "_TREE")
        # And add the package to the LOCAL file
        local_file = open("." + self.config["branches"][pkg_branch]["path"] + "/LOCAL", "a")
        # We write the package name, the version and the date of installation (formatted like this: YYYY-MM-DD)
        local_file.write(self.pkg + " " + pkg_infos["version"] + " " + os.popen("date +%Y-%m-%d").read().strip() + "\n")
        local_file.close()
        # We remove the archive
        os.remove(self.pkg + "-" + pkg_infos["version"] + ".tar.xz")
        # Get the package install infos
        pkg_install_infos = self.pkggetter.get_pkg_install_infos(root=self.root)
        # And execute the post-install ("post" field) script if it exists
        if "post" in pkg_install_infos:
            if self.verbose:
                self.logger.log_info("Executing post-install script...")

            # If root is not /, we need to chroot
            real_root = os.open("/", os.O_RDONLY)
            if self.root != "/":
                os.chdir(self.root)
                os.chroot(self.root)
            
            os.system(pkg_install_infos["post"])

            # If root is not /, we need to exit chroot
            if self.root != "/":
                os.fchdir(real_root)
                os.chroot(".")
            
            os.close(real_root)
            os.chdir(self.root)


        if self.verbose:
            self.logger.log_success("Package " + self.pkg + " installed.")

    def is_installed(self, root="/"):
        pkg_branch = self.pkggetter.get_pkg_branch(root=root)
        if os.path.exists(root + self.config["branches"][pkg_branch]["path"] + "/LOCAL"):
            local_file = open(root + self.config["branches"][pkg_branch]["path"] + "/LOCAL", "r")
            for line in local_file.readlines():
                if line.split(" ")[0].strip() == self.pkg:
                    return True
            return False
        else:
            # The LOCAL file doesn't exist, so the package is not installed but create LOCAL file
            open(root + self.config["branches"][pkg_branch]["path"] + "/LOCAL", "w").close()
            return False