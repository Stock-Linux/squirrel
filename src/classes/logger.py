class Logger:
    def __init__(self):
        self.FAILING_COLOR = "\033[91m"
        self.SUCCESS_COLOR = "\033[92m"
        self.INFO_COLOR = "\033[96m"
        self.RESET_COLOR = "\033[0m"
        self.BOLD_COLOR = "\033[1m"
    
    def log_error(self, message):
        print(self.FAILING_COLOR + message + self.RESET_COLOR)
    
    def log_success(self, message):
        print(self.SUCCESS_COLOR + message + self.RESET_COLOR)

    def log_info(self, message):
        print(self.INFO_COLOR + message + self.RESET_COLOR)