from classes.logger import Logger

class PkgParser:
    def __init__(self, filename):
        self.filename =  filename

    def read_pkg_fields(self, pkg_fields, multiline_fields = [], optional_fields = []):
        file = open(self.filename, "r")
        
        data = {}

        registering_multiline_field = False

        for line in file.readlines():
            splitted_line = line.split("=")
            if not registering_multiline_field:
                field = splitted_line[0].strip()
            value = ""
            # We got the field name so we delete it from the splitted_line
            del splitted_line[0]

            if not registering_multiline_field:
                if field not in multiline_fields:
                    value = "=".join(splitted_line).strip()
                    data[field] = value
                else:
                    registering_multiline_field = True
            else:
                if not field in data:
                    data[field] = ""

                if line.strip() != ")":
                    data[field] += line
                else:
                    registering_multiline_field = False

        not_found_fields = []
        for field in pkg_fields:
            if not field in data and not field in optional_fields:
                not_found_fields.append(field)
            elif field in optional_fields and not field in data:
                continue

        if len(not_found_fields) > 0:
            Logger().log_error("The following fields are missing in the package file: " + ", ".join(not_found_fields))
            exit(1)

        return data

    def get_build_infos(self):
        pkg_fields = [
            "name",
            "description",
            "version",
            "url",
            "source",
            "author",
            "packager",
            "build",
            "post"
        ]

        multiline_fields = [
            "build",
            "post"
        ]

        optional_fields = [
            "author",
            "post"
        ]

        return self.read_pkg_fields(pkg_fields, multiline_fields, optional_fields)

    def get_pkg_infos(self):
        pkg_fields = [
            "name",
            "description",
            "version",
            "url",
            "source",
            "author",
            "packager",
            "rundeps"
        ]

        optional_fields = [
            "author",
            "rundeps"
        ]

        return self.read_pkg_fields(pkg_fields, optional_fields = optional_fields)

    def get_pkg_install_infos(self):
        pkg_fields = [
            "name",
            "version",
            "post"
        ]

        multiline_fields = [
            "post"
        ]

        optional_fields = [
            "post"
        ]

        return self.read_pkg_fields(pkg_fields, multiline_fields, optional_fields)