import shutil
import requests

class Downloader:
    def __init__(self, source, dst):
        self.source = source
        self.dst = dst

    def download(self):
        # If source is a local file, copy it
        if self.source.startswith("/"):
            try:
                shutil.copy(self.source, self.dst)
            except shutil.SameFileError:
                pass
        # Or else, download it from the web
        else:
            local_filename = self.dst
            with requests.get(self.source, stream=True) as r:
                r.raise_for_status()
                with open(local_filename, 'wb') as f:
                    for chunk in r.iter_content(chunk_size=8192):
                        f.write(chunk)