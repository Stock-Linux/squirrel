import os
from classes.configreader import ConfigReader
from classes.downloader import Downloader
from classes.pkgparser import PkgParser
from classes.logger import Logger

class PkgGetter:
    def __init__(self, pkg, root = "/"):
        self.pkg = None
        self.logger = Logger()
        self.config = ConfigReader().read_config()
        # Check if the package is in the repository by checking each INDEX file of each branch
        for branch in self.config["branches"]:
            branch_index = open(root + self.config["branches"][branch]["path"] + "/INDEX", "r")
            for line in branch_index.readlines():
                if line.strip().split(" ")[0] == pkg:
                    self.pkg = pkg
                    return
        self.logger.log_error("Package " + pkg + " not found in the repositories.")
        exit(1)
                

    def get_pkg_branch(self, root = "/"):
        for branch in self.config["branches"]:
            branch_index = open(root + self.config["branches"][branch]["path"] + "/INDEX", "r")
            for line in branch_index.readlines():
                if line.strip().split(" ")[0] == self.pkg:
                    return branch

    # WARNING, do not use this function if you're not a package builder
    def get_all_trees(self, root = "/"):
        # Get trees of all packages, located in branch_path/.[package]_TREE and each line represent a file
        trees = {}
        for branch in self.config["branches"]:
            branch_index = open(self.config["branches"][branch]["path"] + "/INDEX", "r")
            for line in branch_index.readlines():
                pkg = line.strip().split(" ")[0]
                if pkg == self.pkg:
                    continue
                if not os.path.exists(root + self.config["branches"][branch]["path"] + "/." + pkg + "_TREE"):
                    pkginfo_downloader = Downloader(self.config["branches"][branch]["url"] + "/." + pkg + "_TREE", root + self.config["branches"][branch]["path"] + "/." + pkg + "_TREE")
                    pkginfo_downloader.download()
                tree_file = open(root + self.config["branches"][branch]["path"] + "/." + pkg + "_TREE", "r")
                trees[pkg] = []
                for line in tree_file.readlines():
                    trees[pkg].append(line.strip())
        return trees

    def get_pkg_infos(self, root="/"):
        pkg_branch = self.get_pkg_branch(root=root)
        if not os.path.exists(root + self.config["branches"][pkg_branch]["path"] + "/" + self.pkg):
            pkginfo_downloader = Downloader(self.config["branches"][pkg_branch]["url"] + "/" + self.pkg, root + self.config["branches"][pkg_branch]["path"] + "/" + self.pkg)
            pkginfo_downloader.download()

        pkgparser = PkgParser(root + self.config["branches"][pkg_branch]["path"] + "/" + self.pkg)
        return pkgparser.get_pkg_infos()
    
    def get_pkg_install_infos(self, root="/"):
        pkg_branch = self.get_pkg_branch(root=root)
        if not os.path.exists(root + self.config["branches"][pkg_branch]["path"] + "/" + self.pkg):
            pkginfo_downloader = Downloader(self.config["branches"][pkg_branch]["url"] + "/" + self.pkg, root + self.config["branches"][pkg_branch]["path"] + "/" + self.pkg)
            pkginfo_downloader.download()

        pkgparser = PkgParser(root + self.config["branches"][pkg_branch]["path"] + "/" + self.pkg)
        return pkgparser.get_pkg_install_infos()

    def get_makedeps(self, root="/"):
        pkg_branch = self.get_pkg_branch(root=root)
        if not os.path.exists(root + self.config["branches"][pkg_branch]["path"] + "/" + self.pkg):
            pkginfo_downloader = Downloader(self.config["branches"][pkg_branch]["url"] + "/" + self.pkg, root + self.config["branches"][pkg_branch]["path"] + "/" + self.pkg)
            pkginfo_downloader.download()

        pkgparser = PkgParser(root + self.config["branches"][pkg_branch]["path"] + "/" + self.pkg)
        if "makedeps" not in pkgparser.get_pkg_infos():
            return []
        return pkgparser.get_pkg_infos()["makedeps"].split(" ")

    def get_rundeps(self, root="/"):
        pkg_branch = self.get_pkg_branch(root=root)
        if not os.path.exists(root + self.config["branches"][pkg_branch]["path"] + "/" + self.pkg):
            pkginfo_downloader = Downloader(self.config["branches"][pkg_branch]["url"] + "/" + self.pkg, root + self.config["branches"][pkg_branch]["path"] + "/" + self.pkg)
            pkginfo_downloader.download()

        pkgparser = PkgParser(root + self.config["branches"][pkg_branch]["path"] + "/" + self.pkg)
        if "rundeps" not in pkgparser.get_pkg_infos():
            return []
        return pkgparser.get_pkg_infos()["rundeps"].split(" ")

    # Write two recursive functions to get all dependencies of a package
    def get_all_makedeps(self, root="/"):
        makedeps = self.get_makedeps(root=root)
        for makedep in makedeps:
            # Get all rundeps of the makedep
            all_rundeps = PkgGetter(makedep, root=root).get_all_rundeps(root=root)
            # Remove duplicates
            all_rundeps = list(dict.fromkeys(all_rundeps))
            makedeps += all_rundeps
        
        # Remove duplicates
        makedeps = list(dict.fromkeys(makedeps))
        makedeps.reverse()
        return makedeps

    def get_all_rundeps(self, root="/"):
        final_tree = []
        rundeps = self.get_rundeps(root=root)
        for rundep in rundeps:
            final_tree += PkgGetter(rundep, root=root).get_all_rundeps(root=root)
            final_tree.append(rundep)
        
        final_tree.reverse()
        return final_tree