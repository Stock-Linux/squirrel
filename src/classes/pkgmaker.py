import os
import shutil
import tempfile
import subprocess as sp
import magic
import re

from elftools.elf.elffile import ELFFile, DynamicSection

from classes.logger import Logger
from classes.configreader import ConfigReader
from classes.downloader import Downloader
from classes.pkginstaller import PkgInstaller
from classes.pkgparser import PkgParser
from classes.pkggetter import PkgGetter

def read_elf_deps(elffile):
    deps = []
    for section in elffile.iter_sections():
        if not isinstance(section, DynamicSection):
            continue

        for tag in section.iter_tags():
            if tag.entry.d_tag == 'DT_NEEDED':
                deps.append(tag.needed)

    return deps

class PkgMaker:
    def __init__(self, pkg):
        self.config = ConfigReader().read_config()
        self.pkggetter = PkgGetter(pkg)
        self.logger = Logger()
        self.branch_path = self.config["branches"][self.pkggetter.get_pkg_branch()]["path"]
        self.pkgparser = PkgParser(self.branch_path + "/" + pkg)
        self.pkg_build_infos = self.pkgparser.get_build_infos()
        
    def prepare(self):
        # Log a message to the user
        self.logger.log_info("Preparing build of package " + self.logger.BOLD_COLOR + self.pkg_build_infos["name"] + self.logger.INFO_COLOR)
        
        # For each make dependency, check if it's installed and install it if not
        for dep in self.pkggetter.get_all_makedeps():
            if not PkgInstaller(dep).is_installed():
                PkgInstaller(dep).install()

        # Get a temporary directory to work into
        self.tempdir = tempfile.mkdtemp()
        os.chdir(self.tempdir)

        # Set up the base structure of the build directory
        os.mkdir("root/")
        os.mkdir("work/")

        # Now let's download the sources of the package
        for source in self.pkg_build_infos["source"].split(" "):
            if not source == "N/A":
                download_dst = source.split("/")[-1]
                source_downloader = Downloader(source, download_dst)
                source_downloader.download()
            
            if source == self.pkg_build_infos["source"].split(" ")[-1] and not source == "N/A":
                os.chdir("work/")
                # Extract source
                os.system("tar -xf ../" + download_dst)

    def build(self):
        # Log a message to the user
        self.logger.log_info("Starting build...")

        # If there is a source in the file, then check if archive has a folder or not
        if not self.pkg_build_infos["source"].split(" ")[-1] == "N/A":
            if len(os.listdir(".")) == 1:
                os.chdir(os.listdir(".")[0])
        
        # Set the PKG, SRC and MAKEFLAGS environment variables for the build script
        if "JOBS" not in os.environ:
            os.environ["JOBS"] = "1"
            
        if os.environ["JOBS"].isnumeric():
            # Set MAKEFLAGS
            self.logger.log_info("MAKEFLAGS: " + ("-j" + os.environ["JOBS"]))
            os.environ["MAKEFLAGS"] = "-j" + os.environ["JOBS"]
            # Set NINJAJOBS
            self.logger.log_info("NINJAJOBS: " + os.environ["JOBS"])
            os.environ["NINJAJOBS"] = os.environ["JOBS"]
        
        os.environ["PKG"] = os.path.abspath(self.tempdir + "/root")
        os.environ["SRC"] = os.path.abspath(self.tempdir)
        
        # Run build script
        ret = sp.run(self.pkg_build_infos["build"], capture_output=True, shell=True)

        # Log build results
        build_log_file = open(os.environ["PKG"] + "/../build.stdout.log", "w")
        build_log_file.write(ret.stdout.decode())
        build_log_file.close()

        err_log_file = open(os.environ["PKG"] + "/../build.stderr.log", "w")
        err_log_file.write(ret.stderr.decode())
        err_log_file.close()

        # Check if the build have succeeded or not
        try:
            ret.check_returncode()
            self.logger.log_success("Successful build.")
        except sp.CalledProcessError:
            print()
            self.logger.log_info("Build directory: " + os.environ["SRC"])
            self.logger.log_info("Error log: " + os.environ["SRC"] + "/build.stderr.log")
            print()
            self.logger.log_error("Build has failed.")
            exit(1)

    def package(self):
        # Log a message to the user
        self.logger.log_info("Packaging package...")

        # Go to the root of the package
        os.chdir(os.environ["PKG"])

        # Get the future run dependencies by checking ELF files of the package
        global_elfdeps = []
        for subdir, dirs, files in os.walk("."):
            for file in files:
                try:
                    if not magic.from_file(os.path.join(subdir, file)).startswith("ELF"):
                        continue
                    if re.search("^ELF.*executable.*not stripped", magic.from_file(os.path.join(subdir, file))):
                        os.system("strip --strip-all " + os.path.join(subdir, file))
                    elif re.search("^ELF.*shared object.*not stripped", magic.from_file(os.path.join(subdir, file))):
                        os.system("strip --strip-unneeded " + os.path.join(subdir, file))
                    elif re.search("current ar archive", string):
                        os.system("strip --strip-debug " + os.path.join(subdir, file))
            
                    elffile = ELFFile(open(os.path.join(subdir, file), "rb"))
                    elfdeps = read_elf_deps(elffile)
                    global_elfdeps.extend(elfdeps)
                except:
                    continue

        global_elfdeps = list(dict.fromkeys(global_elfdeps))
        pkgs_trees = PkgGetter(self.pkg_build_infos["name"]).get_all_trees()
        
        run_deps = []

        for dep in global_elfdeps:
            for pkg in pkgs_trees:
                for tree_file in pkgs_trees[pkg]:
                    if tree_file.split("/")[-1] == dep:
                        if not pkg in run_deps:
                            run_deps.append(pkg)

        if len(run_deps) > 0:
            pkg_file = open(self.branch_path + "/" + self.pkg_build_infos["name"], "a")
            pkg_file.write("rundeps=" + " ".join(run_deps) + "\n")
            pkg_file.close()
        
        # Generate package tree and copy it to the branch path
        os.system("find . -type f,l > " + self.branch_path + "/." + self.pkg_build_infos["name"] + "_TREE")
        shutil.copy(self.branch_path + "/." + self.pkg_build_infos["name"] + "_TREE", ".")
        # Go to the root of the build directory
        os.chdir("..")
        # Compress the package in a tar xz archive and copy it to the branch path
        os.system("tar -cJpf " + self.pkg_build_infos["name"] + "-" + self.pkg_build_infos["version"] + ".tar.xz root")
        shutil.copy(self.pkg_build_infos["name"] + "-" + self.pkg_build_infos["version"] + ".tar.xz", self.branch_path + "/bins/")
        # Log a success message
        self.logger.log_success("Package " + self.logger.BOLD_COLOR + self.pkg_build_infos["name"] + self.logger.RESET_COLOR + self.logger.SUCCESS_COLOR + " has been made.")