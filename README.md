# Squirrel

Squirrel is our powerful package manager, all in one !

# Basic explanation

So to begin with, let's start with the simple help menu of squirrel which can help you to understand how to use commands.

```
Usage:
  squirrel install <name>... [--root=<root>]
  squirrel info <name>
  squirrel make <name>
  squirrel sync [--root=<root>]
  squirrel (-h | --help)
  squirrel --version

Options:
  -h --help     Show this screen.
  --version     Show version.

```

So, let's explain these commands.
As you can see, the first command to learn is the **install** command.
The install command is used to install a single or multiple packages so you have probably guessed that you can specify a list of package as arguments. The --root option is used to install the packages in a specific directory.

**BUT** before that, you need to sync the repos with the simple sync command. The --root option is used in the same goal as the install command.

Then you have the **info** command which can be used to show informations about a package.

And you have the **make** command, used to compile/make a package that you have written or downloaded manually from the repos and put in your local branch path.

# A little bit of configuration to get things work correctly

To use squirrel, you must write a simple configuration file in `/etc/squirrel.conf`. This configuration file should be written following this model:

```

name_of_the_branch url_of_the_branch local_path_of_the_branch

```

And with a working example:

```

base http://repo.stocklinux.org/base /var/squirrel/base

```

The INDEX and the package files of the base branch will be stored in the `/var/squirrel/base` directory and will be downloaded from `http://repo.stocklinux.org/base` url.
PLEASE DON'T WRITE A SLASH AT THE END OF THE URL OR THE BRANCH PATH.

Note: The url can be a local path too (useful when making packages).

# Manual installation of squirrel

- Clone this repo on your computer

- Get the path of your repo

- Write this in `/usr/bin/squirrel`:

```
#!/bin/bash
python3 path_to_repo/src/main.py $@
```

- Give the execution rights to the new file:

```
chmod +x /usr/bin/squirrel
```

- And install Python dependencies:

```
pip install -r path_to_repo/requirements.txt
```

That's it !