"""Squirrel.

Usage:
  squirrel install <name>... [--root=<root>]
  squirrel info <name>
  squirrel search <name>
  squirrel make <name>
  squirrel sync [--root=<root>]
  squirrel (-h | --help)
  squirrel --version

Options:
  -h --help     Show this screen.
  --version     Show version.

"""
from classes.logger import Logger
import os, re

if os.getuid() != 0:
  Logger().log_error("You must be root to use Squirrel.")
  exit(1)

from docopt import docopt
from classes.configreader import ConfigReader
from classes.pkgmaker import PkgMaker
from classes.pkggetter import PkgGetter
from classes.pkginstaller import PkgInstaller
from classes.downloader import Downloader

if __name__ == '__main__':
    arguments = docopt(__doc__, version='Squirrel 0.1.0')
    
    if arguments["make"]:
        pkg = arguments["<name>"]
        pkgmaker = PkgMaker(pkg[0])
        pkgmaker.prepare()
        print()
        pkgmaker.build()
        print()
        pkgmaker.package()

    if arguments["info"]:
      pkg = arguments["<name>"]
      pkggetter = PkgGetter(pkg[0])
      infos = pkggetter.get_pkg_infos()
      print("Name: " + infos["name"])
      print("Version: " + infos["version"])
      print("Description: " + infos["description"])
      print("URL: " + infos["url"])
      print("Author: " + infos["author"])
      print("Packager: " + infos["packager"])
      if len(pkggetter.get_rundeps()) > 0:
        print("Runtime dependencies: " + ", ".join(pkggetter.get_rundeps()))

    if arguments["install"]:
      pkgs = arguments["<name>"]
      for pkg in pkgs:
        if not arguments["--root"] == None:
          pkginstaller = PkgInstaller(pkg, root=arguments["--root"])
        else:
          pkginstaller = PkgInstaller(pkg)
        
        pkginstaller.install()
    
    if arguments["sync"]:
      for branch in ConfigReader().read_config()["branches"]:
        branch_path = ConfigReader().read_config()["branches"][branch]["path"]
        if arguments["--root"] == None:
          arguments["--root"] = "/"
        
        if not os.path.exists(arguments["--root"] + branch_path):
          os.makedirs(arguments["--root"] + branch_path)
        
        os.chdir(arguments["--root"] + branch_path)
        # Get distant INDEX file with our Downloader class
        Downloader(ConfigReader().read_config()["branches"][branch]["url"] + "/INDEX", "INDEX").download()

      # Log success
      Logger().log_success("Synced with remote repositories.")

    if arguments["search"]:
      pkgs = {}
      branches = ConfigReader().read_config()["branches"]
      for branch in branches:
        if os.path.exists(branches[branch]["path"] + "/INDEX"):
          data = open(branches[branch]["path"] + "/INDEX", "r").readlines()
          for line in data:
            pkgs[line.split(' ')[0]] = line.split(' ')[1]

      results_found = False
      for pkg in pkgs:
        if re.search(".*" + arguments["<name>"][0] + ".*", pkg):
          results_found = True
          if not PkgInstaller(pkg).is_installed():
            print("\033[96m" + pkg + " => " + "\033[92m" + pkgs[pkg].strip() + "\033[0m")
          else:
            print("\033[96m" + pkg + " => " + "\033[92m" + pkgs[pkg].strip() + " [installed]" + "\033[0m")

      if not results_found:
        print("\033[91mNo package corresponds to your expression.\033[0m")